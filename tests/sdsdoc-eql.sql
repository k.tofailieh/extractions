with docs as (select d.doc_id,
                     d.doc_file_name,
                     d.doc_tl,
                     d.doc_type,
                     d.tp,
                     d.doc_version,
                     d.lg,
                     d.country,
                     d.ns,
                     d.doc_cr_dt,
                     d.invalidation_dt,
                     d.cr_dt,
                     df.tp       attached_entity_tp,
                     df.attached_entity_id,
                     df.m_ref as attached_m_ref,
                     doctype.de_ke,
                     d.st,
                     customdoctype.ke
              from doc d
                       inner join(select string_agg(distinct f.tp, ',')           tp,
                                         df.docr_dst_id,
                                         string_agg(df.docr_src_id::text, ',') as attached_entity_id,
                                         string_agg(distinct f.m_ref, ',')        m_ref
                                  from fiche f
                                           inner join docr_fiche df on df.docr_src_id = f.m_id and df.docr_tp = 'ATTACHMENT'
                                  where f.tp in ('PRODUCT', 'RAW_MATERIAL', 'DETERGENT')
                                    and f.active <> 0
                                  group by docr_dst_id) df on df.docr_dst_id = d.doc_id
                       left join lateral (
                  select string_agg(de.de_ke, ',') as de_ke
                  from der_doc dd
                           inner join de on de.de_id = dd.der_dst_id
                  where dd.der_src_id = d.doc_id
                    and dd.der_tp = 'DOC_TYPE'
                  ) doctype on true
                       left join lateral (
                  select lov.ke as ke
                  from entr_doc_lov edl
                           inner join lov on edl.entr_dst_id = lov.lov_id
                  where edl.entr_src_id = d.doc_id
                    and edl.entr_tp = 'CUSTOM_DOC_TYPE'
                  ) customdoctype on true
              where st <> 'ARCHIVED'
                and doc_file_name is not null
              union all
              select d.doc_id,
                     d.doc_file_name,
                     d.doc_tl,
                     d.doc_type,
                     d.tp,
                     d.doc_version,
                     d.lg,
                     d.country,
                     d.ns,
                     d.doc_cr_dt,
                     d.invalidation_dt,
                     d.cr_dt,
                     'SKU'                   as attached_entity_tp,
                     CAST(p.part_id as TEXT) as attached_entity_id,
                     p.name_1                as attached_m_ref,
                     null                    as de_ke,
                     d.st,
                     null                    as ke
              from doc d
                       join docr_part dp on dp.docr_dst_id = d.doc_id
                       join part p on p.part_id = dp.docr_src_id
              where p.tp = 'SKU'
              union all
              select d.doc_id,
                     d.doc_file_name,
                     d.doc_tl,
                     d.doc_type,
                     d.tp,
                     d.doc_version,
                     d.lg,
                     d.country,
                     d.ns,
                     d.doc_cr_dt,
                     d.invalidation_dt,
                     d.cr_dt,
                     'ORGANISATION' as attached_entity_tp,
                     null           as attached_entity_id,
                     null           as attached_m_ref,
                     null           as de_ke,
                     d.st,
                     null           as ke
              from doc d
                       inner join docr_dir on docr_dir.docr_dst_id = d.doc_id and docr_dir.docr_tp = 'ATTACHMENT'
                       inner join dir on dir.dir_id = docr_dir.docr_src_id and dir.dn = 'o=EcoMundo,dc=ecodis,dc=org'
              where st <> 'ARCHIVED'
                and doc_file_name is not null)
select *
from docs

--order by doc_tl asc
;

select distinct tp
from doc;

select * from import.import_line where imp_ref = :imp_ref;