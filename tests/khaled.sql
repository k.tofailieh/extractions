with
rm_names as (
  select value7 as rm_name, value20 rm_code from import.import_line
  where imp_ref = 'ImportComposition_ImportCosmeticProduct_4dae9113-3bb3-479c-b6c3-79417770a659_Product_CO' and coalesce(value7,'') != ''
  and coalesce(value20,'') != ''
  group by value7,value20
),
existingRawMat as (
  select m_id,f1.m_ref as m_ref, marguage
  from fiche f1
  where f1.tp ='RAW_MATERIAL'
  and (split_part(f1.ns,'/',1) = 'EcoMundo' ) /*fix bug getting from another organization*/
  and f1.active <> 0
  and (m_ref, marguage) in (select rm_name, rm_code from rm_names)
)
update import.import_line
set value_id1 = m_id
from existingRawMat
where imp_ref = 'ImportComposition_ImportCosmeticProduct_4dae9113-3bb3-479c-b6c3-79417770a659_Product_CO'
and coalesce(value7,'') != ''
and coalesce(value20,'') != ''
and  value7 = m_ref
and value20 = marguage;

with
existingRawMat as (
  select max(m_id) m_id,f1.m_ref as m_ref
  from fiche f1
  where f1.tp ='RAW_MATERIAL'
  and (split_part(f1.ns,1) = 'EcoMundo' )
  and f1.active <> 0
  and  coalesce(marguage,'') = ''
  group by m_ref
)
update import.import_line
set value_id1 = m_id
from existingRawMat
where imp_ref = 'ImportComposition_ImportCosmeticProduct_4dae9113-3bb3-479c-b6c3-79417770a659_Product_CO'
and coalesce(value7,'') != ''
and coalesce(value20,'') = ''
and  value7 = m_ref
;

update import.import_line
set imp_err = true, imp_err_msg = coalesce(imp_err_msg  ' | ','')  'This RM is not found in database.'
where imp_ref = 'ImportComposition_ImportCosmeticProduct_4dae9113-3bb3-479c-b6c3-79417770a659_Product_CO'
and coalesce(value7,'') != ''
and coalesce(value8,value12,'0.0') = '0.0'
and coalesce(value_id1,0) = 0;




UPDATE import.import_line
SET imp_err = true, imp_err_msg = coalesce(imp_err_msg  ' | ','')  '%MP dans PF and %Ing dans MP and %Ing dans PF are filled, you must empty one of them'
WHERE imp_ref = 'ImportComposition_ImportCosmeticProduct_4dae9113-3bb3-479c-b6c3-79417770a659_Product_CO'
AND coalesce(value8,'0.0') != '0.0'
AND coalesce(value12,'0.0') != '0.0'
AND coalesce(value21,'0.0') != '0.0';


UPDATE import.import_line
SET imp_err = true, imp_err_msg = coalesce(imp_err_msg  ' | ','')  '%MP dans PF and %Ing dans MP and %Ing dans PF are empty, you must filled two of them'
WHERE imp_ref = 'ImportComposition_ImportCosmeticProduct_4dae9113-3bb3-479c-b6c3-79417770a659_Product_CO'
AND coalesce(value8,'0.0') = '0.0'
AND coalesce(value12,'0.0') = '0.0'
AND coalesce(value21,'0.0') = '0.0';

UPDATE import.import_line
SET imp_err = true, imp_err_msg = coalesce(imp_err_msg  ' | ','')  'only one of (%MP dans PF, %Ing dans MP, %Ing dans PF) is filled, you must filled two of them'
WHERE imp_ref = 'ImportComposition_ImportCosmeticProduct_4dae9113-3bb3-479c-b6c3-79417770a659_Product_CO'
AND (
    (coalesce(value8,'0.0') != '0.0' and coalesce(value12,value21,'0.0') = '0.0')
    or (coalesce(value12,'0.0') != '0.0' and coalesce(value8,value21,'0.0') = '0.0')
)

select value_id3,new_id,value_id1,internal_id, * from import.import_line
                                          where imp_ref=:imp_ref
and coalesce(value_id1,0) > 0
---------------------------------------------------------------------------------------------------------------

select * from fiche
         inner join entr_fiche_nomclit on fiche.m_id = entr_fiche_nomclit.entr_src_id
             where m_id='515353'
with
   distinct_new_Product_to_be_inserted as (
      select distinct value1 as Product_name, coalesce(cust_id,'-') Product_code
      from import.import_line
      where internal_id is null and imp_ref = :imp_ref
      AND NOT(COALESCE(imp_err, FALSE)))
,  new_Product_with_new_ids as (select -nextval('seq_fiche_id') as newId , Product_name, Product_code from distinct_new_Product_to_be_inserted)
update import.import_line
set internal_id = newId
from new_Product_with_new_ids
where imp_ref = :imp_ref
and value1 = Product_name
and coalesce(cust_id,'-') = Product_code
;

/* Insert New Product*/
insert into fiche (
   m_id,
   m_ref,
   active,
   m_attribute,
   ns,
   tp,
   marguage,
   cust_id)
SELECT
   abs(internal_id) as m_id,
   value1 as m_ref,
   1 as active,
   0 as m_attribute,
   :user_ns,
   'PRODUCT' as tp,
   cust_id as marguage,
   cust_id as custid
from import.import_line
where imp_ref = :imp_ref               /* Current upload */
and coalesce(imp_err,false) = false             /* No error */
and coalesce(internal_id,0) < 0                /* New Product */
group by internal_id,m_ref,cust_id
;
--------------------------------------------------------------------
update import.import_line
set value_id3=NULL
where imp_ref=:imp_ref

with distinct_compo as (
    select internal_id, value_id1,value26, nextval('seq_nomclit_id') new_id
    from import.import_line
    where imp_ref = :imp_ref
    and value_id3 is null
    and coalesce(value_id1,0) > 0
    and coalesce(internal_id,0) < 0
    group by internal_id, value_id1,value26
)
update import.import_line
set value_id3 = distinct_compo.new_id
from distinct_compo
where imp_ref = :imp_ref
and import_line.value_id1 =  distinct_compo.value_id1
and import_line.internal_id = distinct_compo.internal_id
and coalesce(import_line.value26,'-') = coalesce(distinct_compo.value26,'-')
;

----------------------------------------------------------------------------------------


with
   allFormula as (
    select m_id, cust_id,marguage ,m_ref from fiche
    where ns like :user_organisation || '%' and tp = 'COSMETIC_FORMULA'
)
update import.import_line
set internal_id = m_id
from allFormula
where imp_ref = :imp_ref
and coalesce(value2,'') != ''
and value2 = marguage and value1=m_ref
;

/* insert negative ids for the new Formula */
/* Only one ID for each Formula */

with
   new_Formula_with_new_ids as (select value1 as name_formula ,-nextval('seq_fiche_id') as newId  from import.import_line
where internal_id is null and imp_ref = :imp_ref
  AND NOT(COALESCE(imp_err, FALSE))
group by value1
)
update import.import_line
set new_Id = newId
from new_Formula_with_new_ids
where imp_ref = :imp_ref
and  import_line.value1=new_Formula_with_new_ids.name_formula
;


update
   import.import_line
set
   value29 = 'master_line'
from
   (
   select
      imp_id,
      new_id,
      rank() over
(partition by new_id
   order by
      imp_id asc) as ranks
   from
      import.import_line
   where
      imp_ref =:imp_ref) as master
where
   master.imp_id = import_line.imp_id
   and imp_ref = :imp_ref
    and coalesce(abs(import_line.new_id),0)>0
   and master.ranks = 1
;

/* Insert new Formula*/
insert into fiche (
   m_id,
   cust_id,
   m_ref,
    marguage,
    ns,
   tp,
    active,
   co2)
SELECT
   abs(new_Id) as m_id,
   cust_id as cust_id,
   value1 as m_ref,
   value2 as marguage,
   :user_ns,
   coalesce(value_txt10,'COSMETIC_FORMULA') as tp, /*use value_txt10 to  give tp when  you use importformula in nested ecohub*/
    1 as active,
   value14 as co2
from import.import_line
where imp_ref = :imp_ref                  /* Current upload */
and coalesce(imp_err,false) = false             /* No error */
and coalesce(imp_warn,false) = false         /* No warning */
and coalesce(new_id,0) < 0             /* New Formula */
and value29='master_line'
;