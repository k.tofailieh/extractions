
delete from import.import_line where imp_ref = :imp_ref;

select de_id, ke, tp from lov where ke like '%SDS%';

select distinct de_id, de_ke, de_tp, de_co from de where de_tp like '%DOC_TYPE%' and de_ke like '%SDS%';

select distinct der_tp from der_sdsdoc;

insert into de(de_id, de_ke, de_tp, de_li, de_co) values (nextval('seq_de_id'), 'SDS', 'SDS_DOC_TYPE', 'SDS', 'Inserted in order to migrate sdsdoc.') returning de_id;


select * from import.import_line where imp_ref = 'sds-migration-test';


select * from der_doc where der_dst_id = 12362;
