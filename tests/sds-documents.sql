/* get fiche and doc information into temporary table. */
with docs as (select m_id, efs.entr_tp, entr_id, docr_id, doc_id, doc.tp as doc_tp
              from fiche
                       inner join entr_fiche_sdsdoc efs on fiche.m_id = efs.entr_src_id
                       inner join sdsdoc s on efs.entr_dst_id = s.sdsdoc_id
                       inner join docr_sds on s.sdsdoc_id = docr_sds.docr_src_id
                       inner join doc on docr_sds.docr_dst_id = doc.doc_id
              where docr_tp = 'SDS_ORG'
                and doc.tp in ('SDS_ORG', 'ATTACHMENT')
                and s.status in ('CURRENT', 'B-IN_PROGRESS'))
insert
into import.import_line(value_id1, value_id2, value_id3, value_id4, value1, value2, imp_ref)
select m_id, doc_id, entr_id, docr_id, entr_tp, doc_tp, 'sds-migration-test'
from docs
;

/* Flag records to be migrated. */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg, '') || 'no need to copy this record.'
from docr_fiche
         inner join doc on docr_fiche.docr_dst_id = doc.doc_id
where docr_src_id = value_id1
  and docr_dst_id = value_id2
  and docr_tp = 'ATTACHMENT'
  and imp_ref = :imp_ref;


/* Migrate into new version: fiche-> docr_fiche[ATTACHMENT] -> doc[SDS_ORG]. */
insert into docr_fiche(docr_id, docr_src_id, docr_dst_id, docr_tp, docr_co)
select nextval('seq_docr_fiche_id'), value_id1, value_id2, 'ATTACHMENT', 'migrate-sds-test.'
from import.import_line
where imp_ref = :imp_ref
  and not coalesce(imp_err, false);



select tps.do_as_nolog($$
/* get fiche and doc information into temporary table. */
with docs as (select m_id, efs.entr_tp, entr_id, docr_id, doc_id, doc.tp as doc_tp
              from fiche
                       inner join entr_fiche_sdsdoc efs on fiche.m_id = efs.entr_src_id
                       inner join sdsdoc s on efs.entr_dst_id = s.sdsdoc_id
                       inner join docr_sds on s.sdsdoc_id = docr_sds.docr_src_id
                       inner join doc on docr_sds.docr_dst_id = doc.doc_id
              where docr_tp = 'SDS_ORG'
                and doc.tp in ('SDS_ORG', 'ATTACHMENT') and s.status in ('CURRENT', 'B-IN_PROGRESS'))
insert
into import.import_line(value_id1, value_id2, value_id3, value_id4, value1, value2, imp_ref)
select m_id, doc_id, entr_id, docr_id, entr_tp, doc_tp, 'sds-migration-test'
from docs
;

/* Flag records to be migrated. */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg, '') || 'no need to copy this record.'
from docr_fiche
         inner join doc on docr_fiche.docr_dst_id = doc.doc_id
where docr_src_id = value_id1
  and docr_dst_id = value_id2
  and docr_tp = 'ATTACHMENT'
  and imp_ref = :imp_ref;


/* Migrate into new version: fiche -> docr_fiche[ATTACHMENT] -> doc[SDS_ORG]. */
insert into docr_fiche(docr_id, docr_src_id, docr_dst_id, docr_tp, docr_co)
select nextval('seq_docr_fiche_id'), value_id1, value_id2, 'ATTACHMENT', 'migrate-sds-test.'
from import.import_line
where imp_ref = :imp_ref
  and not coalesce(imp_err, false);$$,
                       $$
/* get fiche and doc information into temporary table. */
with docs as (select m_id, efs.entr_tp, entr_id, docr_id, doc_id, doc.tp as doc_tp
              from fiche
                       inner join entr_fiche_sdsdoc efs on fiche.m_id = efs.entr_src_id
                       inner join sdsdoc s on efs.entr_dst_id = s.sdsdoc_id
                       inner join docr_sds on s.sdsdoc_id = docr_sds.docr_src_id
                       inner join doc on docr_sds.docr_dst_id = doc.doc_id
              where docr_tp = 'SDS_ORG'
                and doc.tp in ('SDS_ORG', 'ATTACHMENT') and s.status in ('CURRENT', 'B-IN_PROGRESS'))
insert
into import.import_line(value_id1, value_id2, value_id3, value_id4, value1, value2, imp_ref)
select m_id, doc_id, entr_id, docr_id, entr_tp, doc_tp, 'sds-migration-test'
from docs
;

/* Flag records to be migrated. */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg, '') || 'no need to copy this record.'
from docr_fiche
         inner join doc on docr_fiche.docr_dst_id = doc.doc_id
where docr_src_id = value_id1
  and docr_dst_id = value_id2
  and docr_tp = 'ATTACHMENT'
  and imp_ref = :imp_ref;


/* Migrate into new version: fiche -> docr_fiche[ATTACHMENT] -> doc[SDS_ORG]. */
insert into docr_fiche(docr_id, docr_src_id, docr_dst_id, docr_tp, docr_co)
select nextval('seq_docr_fiche_id'), value_id1, value_id2, 'ATTACHMENT', 'migrate-sds-test.'
from import.import_line
where imp_ref = :imp_ref
  and not coalesce(imp_err, false);$$,
                       'nolog_password', -- please modify it
                       'nolog_user', -- please modify it
                       'Migrate SDS'),
       "RunOnMaster"();
