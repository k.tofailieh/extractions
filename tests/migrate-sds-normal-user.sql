/* Insert Descriptor if not exist.*/
--insert into de(de_id, de_ke, de_tp, de_li, de_co) values (nextval('seq_de_id'), 'SDS', 'SDS_DOC_TYPE', 'SDS', 'Inserted in order to migrate sdsdoc.') returning de_id;

/* get fiche and doc information into temporary table. */
with DocsToMigrate as (select m_id, efs.entr_tp, entr_id, sdsdoc_id, docr_id, doc_id, doc.tp as doc_tp
                       from fiche
                               inner join entr_fiche_sdsdoc efs on fiche.m_id = efs.entr_src_id
                               inner join sdsdoc s on efs.entr_dst_id = s.sdsdoc_id
                               inner join docr_sds on s.sdsdoc_id = docr_sds.docr_src_id
                               inner join doc on docr_sds.docr_dst_id = doc.doc_id
                      where docr_tp = 'SDS_ORG'
                        and doc.tp in ('SDS_ORG', 'ATTACHMENT')
                      -- and s.status in ('CURRENT', 'B-IN_PROGRESS')
                       )
insert
into import.import_line(value_id1, value_id2, value_id3, value_id4, value_id5, value1, value2, imp_ref)
select m_id,
       doc_id,
       sdsdoc_id,
       docr_id,
       entr_id,
       entr_tp,
       doc_tp,
       'sds-migration-test'
from DocsToMigrate
;

/* Get de_id, to which ke=SDS and tp=SDS_DOC_TYPE. */
update import.import_line
set value_id6 = de_id
from de
where de_ke = 'SDS'
  and de_tp = 'SDS_DOC_TYPE'
  and imp_ref = :imp_ref
;

/* Get sdsdoc RR, to link them with new doc. */
with DocsRR as (select imp_id, de_id, de_tp, de_ke
                from import.import_line
                         inner join der_sdsdoc on value_id3 = der_sdsdoc.der_src_id and der_tp = 'VALID_FOR_REGULATORY'
                         inner join de on der_sdsdoc.der_dst_id = de.de_id

                where imp_ref = :imp_ref)
insert into import.import_matching(imp_id, entr_dst_id, entr_tp, dst_table)
select imp_id, de_id, 'VALID_FOR_REGULATORY', 'de'
from DocsRR;

/* Error for records that wont be migrated (docr_fiche[ATTACHMENT] is already exist). */
update import.import_line
set imp_err     = true,
    imp_err_msg = coalesce(imp_err_msg, '') || 'docr_fiche[ATTACHMENT] for this record already exists.'
from docr_fiche
where docr_src_id = value_id1
  and docr_dst_id = value_id2
  and docr_tp = 'ATTACHMENT'
  and imp_ref = :imp_ref;


/* Warning for records for which der_doc[DOC_TYPE] already exists. */
update import.import_line
set imp_warn     = true,
    imp_warn_msg = coalesce(imp_warn_msg, '') || 'der_doc[DOC_TYPE] for this record already exists.'
from der_doc
where der_src_id = value_id2
  and der_dst_id = value_id6
  and der_tp = 'DOC_TYPE'
  and imp_ref = :imp_ref;


/* Flag RR that wont be migrated(to avoid duplicate relation doc<->RR). */
update import.import_matching matching
set imp_err     = True,
    imp_err_msg = coalesce(matching.imp_err_msg, '') || 'der_doc[VALID_FOR_REGULATORY] for this record already exists.'
from import.import_line line
where line.imp_id = matching.imp_id
  and exists(
        select 1
        from der_doc
        where value_id2 = der_doc.der_src_id and entr_dst_id = der_dst_id and der_tp = 'VALID_FOR_REGULATORY'
    )
;

/* Migrate into new version: fiche-> docr_fiche[ATTACHMENT] -> doc[SDS_ORG]. */
insert into docr_fiche(docr_id, docr_src_id, docr_dst_id, docr_tp, docr_co)
select nextval('seq_docr_fiche_id'), value_id1, value_id2, 'ATTACHMENT', 'migrate-sds-test.'
from import.import_line
where imp_ref = :imp_ref
  and not coalesce(imp_err, false)
group by value_id1, value_id2
on conflict do nothing
;

/* Link Docs with their true RR */
insert into der_doc(der_id, der_src_id, der_dst_id, der_tp, der_co)
select nextval('seq_der_doc_id'), value_id2, value_id6, 'DOC_TYPE', 'Inserted in order to migrate sds.'
from import.import_line line
where imp_ref = :imp_ref
  and not coalesce(imp_warn, false)
  and value_id2 is not null
  and value_id6 is not null
group by value_id2, value_id6
on conflict do nothing;


/* Link Docs with their true RR */
insert into der_doc(der_id, der_src_id, der_dst_id, der_tp, der_co)
select nextval('seq_der_doc_id'), value_id2, entr_dst_id, 'VALID_FOR_REGULATORY', 'Inserted in order to migrate sds.'
from import.import_line line
         inner join import.import_matching matching using (imp_id)
where imp_ref = :imp_ref
  and not coalesce(matching.imp_err, false)
group by value_id2, entr_dst_id
on conflict do nothing;

