WITH clientproducts AS (SELECT DISTINCT d.cn       AS client_name,
                                        f.m_id     AS product_id,
                                        f.m_ref    AS product_name,
                                        f.marguage AS product_code
                        FROM fiche f
                                 INNER JOIN entr_fiche_dir efd ON f.m_id = efd.entr_src_id AND f.tp = 'PRODUCT'
                                 INNER JOIN dir d ON d.dir_id = efd.entr_dst_id
                        WHERE d.cn ILIKE ANY (VALUES ('it_selenium'))),
     products_info AS (SELECT f.m_id AS product_id, de.de_ke AS product_status
                       FROM fiche f
                                INNER JOIN der_fiche df ON f.m_id = df.der_src_id
                                INNER JOIN de ON df.der_dst_id = de.de_id
                       WHERE df.der_tp = 'COSMETIC_PRODUCT_STATE'),
     products_rawmaterials AS (SELECT f.m_id AS product_id, f2.m_id AS RM_id, f2.m_ref AS "RM_name"
                               FROM fiche f
                                        INNER JOIN entr_fiche_nomclit efn ON efn.entr_src_id = f.m_id AND f.tp = 'PRODUCT'
                                        INNER JOIN entr_nomclit_fiche enf ON enf.entr_src_id = efn.entr_dst_id
                                        INNER JOIN fiche f2 ON enf.entr_dst_id = f2.m_id AND f2.tp = 'RAW_MATERIAL')
SELECT cp.client_name,
       cp.product_name,
       COALESCE(cp.product_code, '') AS product_code,
       pinf.product_status,
       COALESCE(prm."RM_name", '')   AS "RM"
FROM clientproducts cp
         INNER JOIN products_info pinf ON cp.product_id = pinf.product_id
         LEFT JOIN products_rawmaterials prm ON prm.product_id = pinf.product_id
ORDER BY cp.client_name, cp.product_name, product_code, pinf.product_status, "RM"



-------------------------------------------------------------------------------------------------------------------------------------------


/* COSMETICs Product info */
SELECT split_part(prod.ns, '/', 1)       AS org,
       prod.marguage                     AS product_code,
       prod.m_ref                        AS product_name,
       prodnomenclature.conc_real_ubound AS rawmaterial_concentration,
       rawmatarial.marguage              AS rawmaterial_code,
       rawmatarial.m_ref                 AS rawmaterial_name

FROM fiche prod
/*Get the product composition (RMs --> Ingredients --> INCIs )*/
         INNER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_DIRECT_COMPO_RM
                    ON ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_src_id = prod.m_id AND
                       ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_tp = 'DIRECT_COMPO'
         INNER JOIN nomclit prodnomenclature
                    ON prodnomenclature.nomclit_id = ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_dst_id
         INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS_RM
                    ON ENTR_nomclit_fiche_HAS_RM.entr_src_id = prodnomenclature.nomclit_id AND
                       ENTR_nomclit_fiche_HAS_RM.entr_tp = 'HAS'

         INNER JOIN fiche rawmatarial ON rawmatarial.m_id = ENTR_nomclit_fiche_HAS_RM.entr_dst_id
         INNER JOIN entr_fiche_nomclit ENTR_fiche_nomclit_DIRECT_COMPO_ING
                    ON rawmatarial.m_id = ENTR_fiche_nomclit_DIRECT_COMPO_ING AND
                       ENTR_fiche_nomclit_DIRECT_COMPO_ING.entr_tp = 'DIRECT_COMPO'
         INNER JOIN nomclit RM_nomclit ON ENTR_fiche_nomclit_DIRECT_COMPO_ING.entr_dst_id = RM_nomclit.nomclit_id
         INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS_ING
                    ON ENTR_nomclit_fiche_HAS_ING.entr_src_id = RM_nomclit.nomclit_id AND
                       ENTR_nomclit_fiche_HAS_ING.entr_tp = 'HAS'

         INNER JOIN fiche ingredient ON ingredient.m_id = ENTR_nomclit_fiche_HAS_ING.entr_dst_id AND
                                        ENTR_nomclit_fiche_HAS_ING.entr_tp = 'HAS' AND
                                        ingredient.tp IN ('INGREDIENT', 'ALLERGEN')
         INNER JOIN anx.entr_fiche_inci
WHERE split_part(prod.ns, '/', 1) IN ('Alvend', 'grupoboticario')
  AND prod.tp = 'PRODUCT'
ORDER BY org, prod.m_ref, rawmatarial.m_ref, prodnomenclature.conc_real_ubound DESC;


SELECT DISTINCT tp
FROM fiche


/* COSMETICs Product info */
SELECT split_part(prod.ns, '/', 1)       AS org,
       prod.marguage                     AS product_code,
       prod.m_ref                        AS product_name,
       prod.cas                          AS product_cas,
       naml                              AS substance_naml,
       s.cas                             AS substance_cas,
       s.ec                              AS substance_ec,
       prodnomenclature.conc_real_ubound AS concentration,
       prodnomenclature.alt_set          AS shade,
       dir.cn                            AS manufacorer_cn,
       dir.displayname                   AS manufacorer_name

FROM fiche prod
/*Get the product composition (RMs --> Ingredients --> INCIs )*/
         INNER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_REAL_COMPO
                    ON ENTR_fiche_nomclit_REAL_COMPO.entr_src_id = prod.m_id AND
                       ENTR_fiche_nomclit_REAL_COMPO.entr_tp = 'REAL_COMPO'
         INNER JOIN nomclit prodnomenclature
                    ON prodnomenclature.nomclit_id = ENTR_fiche_nomclit_REAL_COMPO.entr_dst_id
         INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS_ING
                    ON ENTR_nomclit_fiche_HAS_ING.entr_src_id = prodnomenclature.nomclit_id AND
                       ENTR_nomclit_fiche_HAS_ING.entr_tp = 'HAS'
         INNER JOIN fiche composition
                    ON ENTR_nomclit_fiche_HAS_ING.entr_dst_id = composition.m_id AND
                       composition.tp IN ('INGREDIENT', 'ALLERGEN', 'PERFUME_COMPONENT')
         INNER JOIN entr_fiche_substances efs ON composition.m_id = efs.entr_src_id AND
                                                 efs.entr_tp IN ('INGREDIENT', 'ALLERGEN', 'PERFUME_COMPONENT')
         INNER JOIN substances s ON efs.entr_dst_id = s.sub_id

         INNER JOIN entr_fiche_dir efd ON prod.m_id = efd.entr_src_id AND efd.entr_tp = 'CREATOR'
         INNER JOIN dir ON efd.entr_dst_id = dir.dir_id

WHERE prod.m_id = :m_id


