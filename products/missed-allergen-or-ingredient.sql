SELECT distinct prod.m_id, prod.m_ref, prodnomenclature.conc_real_ubound

FROM fiche prod
/*Get the product composition (RMs --> Ingredients --> INCIs )*/
         INNER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_DIRECT_COMPO_RM
                    ON ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_src_id = prod.m_id AND
                       ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_tp = 'REAL_COMPO'
         INNER JOIN nomclit prodnomenclature
                    ON prodnomenclature.nomclit_id = ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_dst_id
         INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS_RM
                    ON ENTR_nomclit_fiche_HAS_RM.entr_src_id = prodnomenclature.nomclit_id --AND
                    --   ENTR_nomclit_fiche_HAS_RM.entr_tp = 'HAS'

         INNER JOIN fiche ING ON ING.m_id = ENTR_nomclit_fiche_HAS_RM.entr_dst_id
         left join anx.entr_fiche_inci on ING.m_id = entr_fiche_inci.entr_src_id
         left join anx.inci on entr_fiche_inci.entr_dst_id = inci.inci_id
         where inci_id is null
;


select distinct docr_tp from docr_fiche