WITH setSelected as (
  select inforeqset_id from inforeqset
  where inforeqset_cmt ilike :org
), suppliers as (
  select supplier.dir_id, supplier.cn, supplier.displayname, entr_char1
  from dir org
  inner join entr_dir_dir on org.dir_id = entr_src_id and entr_tp = 'SUPPLIER'
  inner join dir supplier on entr_dst_id = supplier.dir_id
  where org.cn = :org and org.dn like ('%,dc=org%')
),
response as (
    select epr.entr_src_id as part_id, reg.reg_ke, epr.entr_st
    from entr_part_reg epr
    inner join reg reg on epr.entr_dst_id = reg.reg_id
    where epr.entr_tp = 'REGULATION'
)
select
  inforeqset_id,
  part.part_id as part_id,
  d1.dir_id as supplier_id,
  d3.dir_id as contact_id,
  inforeq_id,
  d1.cn as supplier_account,
  d1.displayname as supplier_name,
  d1.entr_char1 as supplier_code,

  part.name_1 as article_name,
  part.reference_1 as article_code,

  part.name_2 as supplier_article_name,
  part.reference_2 as supplier_article_code,

  (CASE when coalesce(particular_case.de_ke,'_FALSE') = '_FALSE' then FALSE else TRUE end) as particular_case_st,
  coalesce(particular_case.de_co,'') as particular_case_comment,
  part.active as reference_activation,

  di.der_dt1 as answerDate,
  di.der_dt2 as listDate,
  di.der_co as comment,
  coalesce(di.der_char1, '') as scip_uuid,

  (case
    when response.entr_st = 'REGULATION_COMPLIANCE' and response.reg_ke = 'ROHS 2015' then 'Compliant'
    when response.entr_st = 'REGULATION_NON_COMPLIANCE' and response.reg_ke = 'ROHS 2015' then 'Non-compliant'
    when response.entr_st = 'REGULATION_COMPLIANCE_EXEMPTION' and response.reg_ke = 'ROHS 2015' then 'RoHS compliant by exemption'
    when response.entr_st = 'REGULATION_NOT_CONCERNED' and response.reg_ke = 'ROHS 2015' then 'Not concerned'

    else 'No response'
    end) as roh_2015_response,
   ( case
    when response.entr_st = 'REGULATION_NON_COMPLIANCE' and response.reg_ke = 'SVHC' then 'SVHC reported'

    when response.entr_st = 'REGULATION_COMPLIANCE' and response.reg_ke = 'SVHC' then 'No SVHC'
    else 'No response' end
      ) as svhc_response,
   -- substances
  substances.sub_id substanceId,
  substances.cas substanceCas,
  substances.ec substanceEc,
  substances.naml as substanceName,

 --documents
 document.doc_file_name as fileName,
 document.doc_id as documentId,
 document.cr_dt as documentDate,
 document.doc_version as DocumentVersion,
 document.lg as documnetLanguage,

  d4.dir_id as organizationId,
  d4.cn as organizationAccount,
  d4.displayname as organizationName


from inforeqset
inner join entr_inforeqset_inforeq eii on eii.entr_src_id = inforeqset_id
inner join inforeq on inforeq_id = eii.entr_dst_id and inforeq_st = 'VALID'
inner join entr_inforeq_part eip on eip.entr_src_id = inforeq_id
inner join part on part_id = eip.entr_dst_id

inner join entr_part_dir epd on epd.entr_src_id = part_id
inner join suppliers d1 on d1.dir_id = epd.entr_dst_id
inner join entr_inforeq_dir eid on eid.entr_src_id = inforeq_id and eid.entr_tp = 'ASKED'
inner join dir d3 on d3.dir_id = eid.entr_dst_id
left join entr_dir_dir edd2 on edd2.entr_dst_id = d3.dir_id and edd2.entr_tp = 'MEMBER'
left join dir d4 on d4.dir_id = edd2.entr_src_id
left join entr_part_part epp on epp.entr_src_id = part.part_id

-- get particular case.
left join der_part on part.part_id = der_part.der_src_id and der_part.der_tp = 'PARTICULARCASE'
left join de particular_case on der_part.der_dst_id = particular_case.de_id

-- get Conformity (by Compliance)
left join der_inforeq di ON inforeq.inforeq_id = di.der_src_id
left join de  on de.de_id = di.der_dst_id

-- get svhc - rohs 2015.
left join response on response.part_id = part.part_id


-- get contained substances.
left join entr_part_substances on part.part_id = entr_part_substances.entr_src_id
left join substances on entr_part_substances.entr_dst_id = substances.sub_id

-- get Document (by Compliance):
left join docr_part on part.part_id = docr_part.docr_src_id
left join doc  document on docr_part.docr_dst_id = document.doc_id

where inforeqset_id = :inforeqset_id and inforeqset_id in (select inforeqset_id from setSelected) and part.ns = :org
order by part.name_1;


 -- deke valid + derchar1 = Presence of SVHC >0,1%
 -- deke valid = No SVHC >0,1%\
 -- deke invalid = No response was obtained.

--







