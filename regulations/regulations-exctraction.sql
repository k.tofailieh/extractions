select reg_id,reg_desc,name,reg_ke,ns,nbRegEntries,nbRegSub,nbRegSet
from reg				
inner join (select reg_id, count(entr_id) as nbRegEntries from reg left join entr_reg_regentry on entr_src_id = reg_id group by reg_id) reg1 using (reg_id)				
inner join (select reg_id, count(entr_id) as nbRegSub from reg left join entr_reg_substances on entr_src_id = reg_id group by reg_id) reg2 using (reg_id)				
inner join (select reg_id, count(entr_id) as nbRegSet from reg left join entr_reg_subset on entr_src_id = reg_id group by reg_id) reg3 using (reg_id)
where reg.name not in ('Nouvelle réglementation', 'New regulation', 'NewRegulation')
and reg.name not ilike '%test%'			
order by nbRegEntries, nbRegSub+nbRegSet;

select inci_id,inciname,  reg_ke
    from anx.inci
    join anx.entr_regentry_inci eri on inci.inci_id = eri.entr_dst_id
    join entr_reg_regentry err on eri.entr_src_id = err.entr_dst_id
    join reg on err.entr_src_id = reg.reg_id