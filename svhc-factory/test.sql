select
                -- supplier contact info.
				supplier.dir_id as supplier_id,
				supplier.displayname as supplier_name,
				edd2.entr_char1 as supplier_code,
				supplier.dir_dt1 as supplier_createion_date,
				supplier.dir_dt2 as supplier_last_modification_date,

				contact.dn group_id,
				contact.dir_id as contact_id,
				contact.displayname as contact_name,
				contact.cn as contact_account,
				contact.email as contact_email,

                -- particular case and supplier activity.
                (CASE when coalesce(particular_case.de_ke,'_FALSE') = '_FALSE' then FALSE else TRUE end) as particular_case_st,
                (CASE when coalesce(edd2.entr_st, 'ACTIVE') = 'ACTIVE' or edd2.entr_st = 'VALID' then True else FALSE end) as supplier_activity,

                -- response counts
                numpart.total_request_count,
                numpart.activate_requested_references_count,
                numpart.deactivate_requested_references_count,

				numpart.svhc_request_count,
				numpart.rohs_request_count,
				numpart.otherformcm_request_count,
				numpart.otherformcm_reported,
				numpart.redphosphorus_request_count,
				numpart.prop65_request_count,
				numpart.nanoparticles_request_count,
				numpart.pop_request_count
			from dir supplier
			inner join entr_dir_dir edd2 on edd2.entr_dst_id = supplier.dir_id and edd2.entr_tp = 'SUPPLIER'
			inner join dir org on edd2.entr_src_id = org.dir_id and org.cn = 'Orexad'
			inner join entr_dir_dir edd on edd.entr_src_id = supplier.dir_id and edd.entr_tp = 'MEMBER'
			inner join dir contact on contact.dir_id = edd.entr_dst_id and exists (
					select 1 from entr_inforeqset_dir eid
					where eid.entr_dst_id = contact.dir_id and eid.entr_tp = 'ASKED'
				)

			inner join (
				select eidreq.entr_dst_id as dir_id,

				                    -- request numbers.
                    count(distinct part_id) as total_request_count,
                    count(distinct (case when coalesce(part.active, 1) =1 then  part_id else null end)) as activate_requested_references_count,
                    count(distinct (case when part.active =0 then  part_id else null end)) as deactivate_requested_references_count,

					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_SVHC_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as svhc_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_ROHS_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as rohs_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_OTHER_ST_02' AND de_ke = 'VALID' THEN 1 ELSE null END) as otherformcm_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_OTHER_ST_02' and di1.der_xml like '%_contained_in_ref2":"yes"%' AND de_ke = 'VALID' THEN 1 ELSE null END) as otherformcm_reported,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_REDPHOSPHORUS_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as redphosphorus_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_PROP65_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as prop65_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_NANOPARTICLES_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as nanoparticles_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_POP_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as pop_request_count
				from entr_inforeq_dir eidreq
				inner join inforeq on inforeq.inforeq_id = eidreq.entr_src_id and inforeq.inforeq_st  = 'VALID'
				inner join entr_inforeqset_inforeq eii on eii.entr_dst_id = inforeq_id and eii.entr_tp = 'INFOREQ_DETAIL'
				inner join entr_inforeq_part eipart on eipart.entr_src_id = inforeq_id and eipart.entr_tp = 'SVHC'
				inner join part on part_id = eipart.entr_dst_id
				left join der_inforeq di1 on di1.der_src_id = inforeq_id
					AND der_tp in ('INFOREQ_FORM_SVHC_ST', 'INFOREQ_FORM_ROHS_ST', 'INFOREQ_FORM_OTHER_ST_02', 'INFOREQ_FORM_REDPHOSPHORUS_ST',
					'INFOREQ_FORM_PROP65_ST', 'INFOREQ_FORM_NANOPARTICLES_ST', 'INFOREQ_FORM_POP_ST')
				left join de on de_id = der_dst_id
				where eidreq.entr_tp = 'ASKED'
				group by eidreq.entr_dst_id
			) numpart on numpart.dir_id = contact.dir_id
left join (
    select eidreq.entr_dst_id as dir_id,
             -- particular case state.
            de_ke, de_co
            from  entr_inforeq_dir eidreq
            inner join entr_inforeq_part eipart on eipart.entr_src_id = eidreq.entr_src_id
            inner join der_part on eipart.entr_dst_id = der_src_id
            inner join de on der_part.der_dst_id = de.de_id
                ) particular_case on particular_case.dir_id = contact.dir_id

			order by supplier.cn;

select * from reg where reg_ke like 'ROHS 2015'




---------------------------


select
				supplier.dir_id as supplier_id,
				supplier.displayname as supplier_name,
				contact.dir_id,
				contact.cn as contact_name,
				edd2.entr_char1 as supplier_code,

				svhc_reported,
				prop65_reported,
				redphosphorus_reported,
				nanoparticles_reported,
				pop_reported,
				rohs2011_reported,
				rohs2015_reported,
				rohs2017_reported,
				rohschina_reported,
				numpart.total_request_count,
				numpart.svhc_request_count,
				numpart.rohs_request_count,
				numpart.otherformcm_request_count,
				numpart.otherformcm_reported,
				numpart.redphosphorus_request_count,
				numpart.prop65_request_count,
				numpart.nanoparticles_request_count,
				numpart.pop_request_count
			from dir supplier
			left join entr_dir_dir edd2 on edd2.entr_dst_id = supplier.dir_id and edd2.entr_tp = 'SUPPLIER' and edd2.entr_src_id = :dirId
			inner join entr_dir_dir edd on edd.entr_src_id = supplier.dir_id and edd.entr_tp = 'MEMBER'
			inner join dir contact on contact.dir_id = edd.entr_dst_id and exists (
					select 1 from entr_inforeqset_dir eid
					where eid.entr_dst_id = contact.dir_id and eid.entr_tp = 'ASKED' and eid.entr_src_id = :inforeqsetId
				)

			inner join (
				select eidreq.entr_dst_id as dir_id,
					count(distinct part_id) as total_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_SVHC_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as svhc_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_ROHS_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as rohs_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_OTHER_ST_02' AND de_ke = 'VALID' THEN 1 ELSE null END) as otherformcm_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_OTHER_ST_02' and di1.der_xml like '%_contained_in_ref2":"yes"%' AND de_ke = 'VALID' THEN 1 ELSE null END) as otherformcm_reported,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_REDPHOSPHORUS_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as redphosphorus_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_PROP65_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as prop65_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_NANOPARTICLES_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as nanoparticles_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_POP_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as pop_request_count
				from entr_inforeq_dir eidreq
				inner join inforeq on inforeq.inforeq_id = eidreq.entr_src_id and inforeq.inforeq_st  = 'VALID'
				inner join entr_inforeqset_inforeq eii on eii.entr_src_id = :inforeqsetId and eii.entr_dst_id = inforeq_id and eii.entr_tp = 'INFOREQ_DETAIL'
				inner join entr_inforeq_part eipart on eipart.entr_src_id = inforeq_id and eipart.entr_tp = 'SVHC'
				inner join part on part_id = eipart.entr_dst_id
				left join der_inforeq di1 on di1.der_src_id = inforeq_id
					AND der_tp in ('INFOREQ_FORM_SVHC_ST', 'INFOREQ_FORM_ROHS_ST', 'INFOREQ_FORM_OTHER_ST_02', 'INFOREQ_FORM_REDPHOSPHORUS_ST',
					'INFOREQ_FORM_PROP65_ST', 'INFOREQ_FORM_NANOPARTICLES_ST', 'INFOREQ_FORM_POP_ST')
				left join de on de_id = der_dst_id
				where eidreq.entr_tp = 'ASKED'
				group by eidreq.entr_dst_id
			) numpart on numpart.dir_id = contact.dir_id
			inner join (
				select eidreq.entr_dst_id as dir_id,
					count(CASE when eps.entr_id is not null and reg_ke not in ('PROP65', 'RED_PHOSPHORUS', 'NANOPARTICLES', 'POP') THEN 1 ELSE null END) as svhc_reported,
					count(CASE when reg_ke = 'PROP65' THEN 1 ELSE null END) as prop65_reported,
					count(CASE when reg_ke = 'RED_PHOSPHORUS' THEN 1 ELSE null END) as redphosphorus_reported,
					count(CASE when reg_ke = 'NANOPARTICLES' THEN 1 ELSE null END) as nanoparticles_reported,
					count(CASE when reg_ke = 'POP' THEN 1 ELSE null END) as pop_reported
				from entr_inforeq_dir eidreq
				inner join inforeq on inforeq.inforeq_id = eidreq.entr_src_id and inforeq.inforeq_st  = 'VALID'
				inner join entr_inforeqset_inforeq eii on eii.entr_src_id = :inforeqsetId and eii.entr_dst_id = inforeq_id and eii.entr_tp = 'INFOREQ_DETAIL'
				inner join entr_inforeq_part eipart on eipart.entr_src_id = inforeq_id and eipart.entr_tp = 'SVHC'
				inner join part on part_id = eipart.entr_dst_id
				left join entr_part_substances eps on eps.entr_src_id = part_id
				left join entr_reg_substances ers on ers.entr_dst_id = eps.entr_dst_id and ers.entr_tp = 'SUBSTANCES'
				left join reg on reg_id = ers.entr_src_id and reg_ke in ('CANDIDATELIST', 'PROP65', 'RED_PHOSPHORUS', 'NANOPARTICLES', 'POP')
				where eidreq.entr_tp = 'ASKED'
				group by eidreq.entr_dst_id
			) substancereported on substancereported.dir_id = contact.dir_id
			inner join (
				select eidreq.entr_dst_id as dir_id,
					count(CASE when reg_ke = 'ROHS 2011' THEN 1 ELSE null END) as rohs2011_reported,
					count(CASE when reg_ke = 'ROHS 2015' THEN 1 ELSE null END) as rohs2015_reported,
					count(CASE when reg_ke = 'ROHS 2017' THEN 1 ELSE null END) as rohs2017_reported,
					count(CASE when reg_ke = 'ROHS China' THEN 1 ELSE null END) as rohschina_reported
				from entr_inforeq_dir eidreq
				inner join inforeq on inforeq.inforeq_id = eidreq.entr_src_id and inforeq.inforeq_st  = 'VALID'
				inner join entr_inforeqset_inforeq eii on eii.entr_src_id = :inforeqsetId and eii.entr_dst_id = inforeq_id and eii.entr_tp = 'INFOREQ_DETAIL'
				inner join entr_inforeq_part eipart on eipart.entr_src_id = inforeq_id and eipart.entr_tp = 'SVHC'
				inner join part on part_id = eipart.entr_dst_id
				left join entr_part_reg epr on epr.entr_src_id = part_id and epr.entr_tp = 'REGULATION' and epr.entr_st = 'REGULATION_NON_COMPLIANCE'
				left join reg on reg_id = epr.entr_dst_id and reg_root_ke = 'ROHS'
				where eidreq.entr_tp = 'ASKED'
				group by eidreq.entr_dst_id
			) rohsreported on rohsreported.dir_id = contact.dir_id
			order by supplier.displayname;





select
                -- supplier contact info.
				supplier.dir_id as supplier_id,
				supplier.displayname as supplier_name,
				edd2.entr_char1 as supplier_code,
				supplier.dir_dt1 as supplier_createion_date,
				supplier.dir_dt2 as supplier_last_modification_date,

				contact.dn group_id,
				contact.dir_id as contact_id,
				contact.displayname as contact_name,
				contact.cn as contact_account,
				contact.email as contact_email,

                -- particular case and supplier activity.
                --(CASE when coalesce(particular_case.de_ke,'_FALSE') = '_FALSE' then FALSE else TRUE end) as particular_case_st,
                (CASE when coalesce(edd2.entr_st, 'ACTIVE') = 'ACTIVE' or edd2.entr_st = 'VALID' then True else FALSE end) as supplier_activity,

                -- response counts
                numpart.total_request_count,
                numpart.activate_requested_references_count,
                numpart.deactivate_requested_references_count,

				numpart.svhc_request_count,
				numpart.rohs_request_count,
				numpart.otherformcm_request_count,
				numpart.otherformcm_reported,
				numpart.redphosphorus_request_count,
				numpart.prop65_request_count,
				numpart.nanoparticles_request_count,
				numpart.pop_request_count,

                -- reported
                svhc_reported,
				prop65_reported,
				redphosphorus_reported,
				nanoparticles_reported,
				pop_reported,
				rohs2011_reported,
				rohs2015_reported,
				rohs2017_reported,
				rohschina_reported
			from dir supplier
			inner join entr_dir_dir edd2 on edd2.entr_dst_id = supplier.dir_id and edd2.entr_tp = 'SUPPLIER'
			inner join dir org on edd2.entr_src_id = org.dir_id and org.cn = :org and org.dn like ('%,dc=org%')
			inner join entr_dir_dir edd on edd.entr_src_id = supplier.dir_id and edd.entr_tp = 'MEMBER'
			inner join dir contact on contact.dir_id = edd.entr_dst_id and exists (
					select 1 from entr_inforeqset_dir eid
					where eid.entr_dst_id = contact.dir_id and eid.entr_tp = 'ASKED' and eid.entr_src_id = :inforeqsetId
				)

			inner join (
				select eidreq.entr_dst_id as dir_id,

				                    -- request numbers.
                    count(distinct part_id) as total_request_count,
                    count(distinct (case when coalesce(part.active, 1) =1 then  part_id else null end)) as activate_requested_references_count,
                    count(distinct (case when part.active =0 then  part_id else null end)) as deactivate_requested_references_count,

					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_SVHC_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as svhc_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_ROHS_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as rohs_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_OTHER_ST_02' AND de_ke = 'VALID' THEN 1 ELSE null END) as otherformcm_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_OTHER_ST_02' and di1.der_xml like '%_contained_in_ref2":"yes"%' AND de_ke = 'VALID' THEN 1 ELSE null END) as otherformcm_reported,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_REDPHOSPHORUS_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as redphosphorus_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_PROP65_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as prop65_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_NANOPARTICLES_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as nanoparticles_request_count,
					count(CASE WHEN di1.der_tp = 'INFOREQ_FORM_POP_ST' AND de_ke = 'VALID' THEN 1 ELSE null END) as pop_request_count
				from entr_inforeq_dir eidreq
				inner join inforeq on inforeq.inforeq_id = eidreq.entr_src_id and inforeq.inforeq_st  = 'VALID'
				inner join entr_inforeqset_inforeq eii on eii.entr_src_id = :inforeqsetId and eii.entr_dst_id = inforeq_id and eii.entr_tp = 'INFOREQ_DETAIL'
				inner join entr_inforeq_part eipart on eipart.entr_src_id = inforeq_id and eipart.entr_tp = 'SVHC'
				inner join part on part_id = eipart.entr_dst_id
				left join der_inforeq di1 on di1.der_src_id = inforeq_id
					AND der_tp in ('INFOREQ_FORM_SVHC_ST', 'INFOREQ_FORM_ROHS_ST', 'INFOREQ_FORM_OTHER_ST_02', 'INFOREQ_FORM_REDPHOSPHORUS_ST',
					'INFOREQ_FORM_PROP65_ST', 'INFOREQ_FORM_NANOPARTICLES_ST', 'INFOREQ_FORM_POP_ST')
				left join de on de_id = der_dst_id
				where eidreq.entr_tp = 'ASKED'
				group by eidreq.entr_dst_id
			) numpart on numpart.dir_id = contact.dir_id
			inner join (
				select eidreq.entr_dst_id as dir_id,
					count(CASE when eps.entr_id is not null and reg_ke not in ('PROP65', 'RED_PHOSPHORUS', 'NANOPARTICLES', 'POP') THEN 1 ELSE null END) as svhc_reported,
					count(CASE when reg_ke = 'PROP65' THEN 1 ELSE null END) as prop65_reported,
					count(CASE when reg_ke = 'RED_PHOSPHORUS' THEN 1 ELSE null END) as redphosphorus_reported,
					count(CASE when reg_ke = 'NANOPARTICLES' THEN 1 ELSE null END) as nanoparticles_reported,
					count(CASE when reg_ke = 'POP' THEN 1 ELSE null END) as pop_reported
				from entr_inforeq_dir eidreq
				inner join inforeq on inforeq.inforeq_id = eidreq.entr_src_id and inforeq.inforeq_st  = 'VALID'
				inner join entr_inforeqset_inforeq eii on eii.entr_src_id = :inforeqsetId and eii.entr_dst_id = inforeq_id and eii.entr_tp = 'INFOREQ_DETAIL'
				inner join entr_inforeq_part eipart on eipart.entr_src_id = inforeq_id and eipart.entr_tp = 'SVHC'
				inner join part on part_id = eipart.entr_dst_id
				left join entr_part_substances eps on eps.entr_src_id = part_id
				left join entr_reg_substances ers on ers.entr_dst_id = eps.entr_dst_id and ers.entr_tp = 'SUBSTANCES'
				left join reg on reg_id = ers.entr_src_id and reg_ke in ('CANDIDATELIST', 'PROP65', 'RED_PHOSPHORUS', 'NANOPARTICLES', 'POP')
				where eidreq.entr_tp = 'ASKED'
				group by eidreq.entr_dst_id
			) substancereported on substancereported.dir_id = contact.dir_id
			inner join (
				select eidreq.entr_dst_id as dir_id,
					count(CASE when reg_ke = 'ROHS 2011' THEN 1 ELSE null END) as rohs2011_reported,
					count(CASE when reg_ke = 'ROHS 2015' THEN 1 ELSE null END) as rohs2015_reported,
					count(CASE when reg_ke = 'ROHS 2017' THEN 1 ELSE null END) as rohs2017_reported,
					count(CASE when reg_ke = 'ROHS China' THEN 1 ELSE null END) as rohschina_reported
				from entr_inforeq_dir eidreq
				inner join inforeq on inforeq.inforeq_id = eidreq.entr_src_id and inforeq.inforeq_st  = 'VALID'
				inner join entr_inforeqset_inforeq eii on eii.entr_src_id = :inforeqsetId and eii.entr_dst_id = inforeq_id and eii.entr_tp = 'INFOREQ_DETAIL'
				inner join entr_inforeq_part eipart on eipart.entr_src_id = inforeq_id and eipart.entr_tp = 'SVHC'
				inner join part on part_id = eipart.entr_dst_id
				left join entr_part_reg epr on epr.entr_src_id = part_id and epr.entr_tp = 'REGULATION' and epr.entr_st = 'REGULATION_NON_COMPLIANCE'
				left join reg on reg_id = epr.entr_dst_id and reg_root_ke = 'ROHS'
				where eidreq.entr_tp = 'ASKED'
				group by eidreq.entr_dst_id
			) rohsreported on rohsreported.dir_id = contact.dir_id
			order by supplier.cn;


    left join (
    select eidreq.entr_dst_id as dir_id,
             -- particular case state.
            de_ke, de_co
            from  entr_inforeq_dir eidreq
            inner join entr_inforeq_part eipart on eipart.entr_src_id = eidreq.entr_src_id
            inner join der_part on eipart.entr_dst_id = der_src_id
            inner join de on der_part.der_dst_id = de.de_id
                ) particular_case on particular_case.dir_id = contact.dir_id



