WITH setSelected as (
  select inforeqset_id from inforeqset
  where inforeqset_cmt ilike :org
), suppliers as (
  select supplier.dir_id, supplier.cn, supplier.displayname, entr_char1 from dir org
  inner join entr_dir_dir on org.dir_id = entr_src_id and entr_tp = 'SUPPLIER'
  inner join dir supplier on entr_dst_id = supplier.dir_id
  where org.cn = :org and org.dn like ('%,dc=org%')
),
responces as (
    select epr.entr_src_id as part_id, reg_ke, entr_st from  entr_part_reg epr
    left join reg on epr.entr_dst_id = reg.reg_id
)
select

  -- BOMinBOM
  p2.part_id as nomclitId,
  p2.name_1 as nomclitName,
  p2.reference_1 as nomclitCode,

  bominbom_compliance.der_char2 as bominbom_complience,
  reference_compliance.der_char2 as reference_complience,


    coalesce(bill_response.reg_ke, '') as responce_key,
    (case
      when bill_response.entr_st = 'REGULATION_COMPLIANCE' then 'Compliant'
      when bill_response.entr_st = 'REGULATION_NON_COMPLIANCE'  then 'Non-compliant'
      when bill_response.entr_st = 'REGULATION_COMPLIANCE_EXEMPTION'  then 'RoHS compliant by exemption'
      when bill_response.entr_st = 'REGULATION_NOT_CONCERNED'  then 'Not concerned'

      when bill_response.entr_st = 'REGULATION_NON_COMPLIANCE' then 'SVHC reported'

      when bill_response.entr_st = 'REGULATION_COMPLIANCE' then 'No SVHC'
      else 'No response'
      end) as responce,
  -- Reference
  p1.part_id as referenceId,
  p1.name_1 as referenceName,
  p1.reference_1 as referenceCode,
  p1.active as referenceActivaion,

  cr_dt as creation_date,
  md_dt as modification_date,
  -- supplier, contact
  d1.dir_id as supplierId, d1.displayname as supplierName, d1.entr_char1 as supplierCode,
  d3.dir_id as contactId, d3.dn as groupId, d3.displayname as contactName, d3.email
from inforeqset
inner join entr_inforeqset_inforeq eii on eii.entr_src_id = inforeqset_id
inner join inforeq on inforeq_id = eii.entr_dst_id and inforeq_st = 'VALID'
inner join entr_inforeq_part eip on eip.entr_src_id = inforeq_id
inner join part p1 on p1.part_id = eip.entr_dst_id

inner join entr_nomclit_part enp on enp.entr_dst_id = p1.part_id and enp.entr_tp  = 'NOMENCLATURE_ITEM_FOR_PART_IS'
inner join nomclit on nomclit_id = enp.entr_src_id and st = 'VALID'
inner join entr_part_nomclit epn on epn.entr_dst_id = nomclit_id and epn.entr_tp = 'CONTAINS'
-- bominbom
inner join part p2 on p2.part_id = epn.entr_src_id
inner join entr_part_dir epd on epd.entr_src_id = p1.part_id
inner join suppliers d1 on d1.dir_id = epd.entr_dst_id
inner join entr_inforeq_dir eid on eid.entr_src_id = inforeq_id and eid.entr_tp = 'ASKED'
inner join dir d3 on d3.dir_id = eid.entr_dst_id

left join entr_inforeq_part eip2 on p2.part_id = eip2.entr_dst_id
-- bill compliance.
left join der_inforeq bominbom_compliance on eip2.entr_src_id = bominbom_compliance.der_src_id
-- bill response.
left join responces bill_response on bill_response.part_id = p2.part_id

-- reference compliance.
left join der_inforeq reference_compliance on inforeq.inforeq_id = reference_compliance.der_src_id
-- reference response.
left join responces reference_response on reference_response.part_id = p1.part_id
where  inforeqset_id in (select inforeqset_id from setSelected)
order by p2.name_1,p1.name_1;







