/* Extract phrases for provided-name client in certain application(bundle): */

SELECT phrac_id                                                AS phrase_id,
       phrac.tp                                                AS phrac_tp,
       phrac.ke                                                AS phrac_key,
       phractxtke.ke                                           AS phractxtke_key,
       max(CASE WHEN lg = 'en' THEN txt ELSE '' END)           AS "English",
       max(CASE WHEN lg = 'en' THEN phractxt_id ELSE NULL END) AS "English_Id",
       max(CASE WHEN lg = 'fr' THEN txt ELSE '' END)           AS "French",
       max(CASE WHEN lg = 'fr' THEN phractxt_id ELSE NULL END) AS "French_Id",
       max(CASE WHEN lg = 'bg' THEN txt ELSE '' END)           AS "Bulgarian",
       max(CASE WHEN lg = 'bg' THEN phractxt_id ELSE NULL END) AS "Bulgarian_Id",
       max(CASE WHEN lg = 'cs' THEN txt ELSE '' END)           AS "Czech",
       max(CASE WHEN lg = 'cs' THEN phractxt_id ELSE NULL END) AS "Czech_Id",
       max(CASE WHEN lg = 'da' THEN txt ELSE '' END)           AS "Danish",
       max(CASE WHEN lg = 'da' THEN phractxt_id ELSE NULL END) AS "Danish_Id",
       max(CASE WHEN lg = 'de' THEN txt ELSE '' END)           AS "German",
       max(CASE WHEN lg = 'de' THEN phractxt_id ELSE NULL END) AS "German_Id",
       max(CASE WHEN lg = 'et' THEN txt ELSE '' END)           AS "Estonian",
       max(CASE WHEN lg = 'et' THEN phractxt_id ELSE NULL END) AS "Estonian_Id",
       max(CASE WHEN lg = 'el' THEN txt ELSE '' END)           AS "Greek",
       max(CASE WHEN lg = 'el' THEN phractxt_id ELSE NULL END) AS "Greek_Id",
       max(CASE WHEN lg = 'es' THEN txt ELSE '' END)           AS "Spanish",
       max(CASE WHEN lg = 'es' THEN phractxt_id ELSE NULL END) AS "Spanish_Id",
       max(CASE WHEN lg = 'fi' THEN txt ELSE '' END)           AS "Finnish",
       max(CASE WHEN lg = 'fi' THEN phractxt_id ELSE NULL END) AS "Finnish_Id",
       max(CASE WHEN lg = 'hr' THEN txt ELSE '' END)           AS "Croatian",
       max(CASE WHEN lg = 'hr' THEN phractxt_id ELSE NULL END) AS "Croatian_Id",
       max(CASE WHEN lg = 'hu' THEN txt ELSE '' END)           AS "Hungarian",
       max(CASE WHEN lg = 'hu' THEN phractxt_id ELSE NULL END) AS "Hungarian_Id",
       max(CASE WHEN lg = 'is' THEN txt ELSE '' END)           AS "Icelandic",
       max(CASE WHEN lg = 'is' THEN phractxt_id ELSE NULL END) AS "Icelandic_Id",
       max(CASE WHEN lg = 'it' THEN txt ELSE '' END)           AS "Italian",
       max(CASE WHEN lg = 'it' THEN phractxt_id ELSE NULL END) AS "Italian_Id",
       max(CASE WHEN lg = 'lt' THEN txt ELSE '' END)           AS "Lithuanian",
       max(CASE WHEN lg = 'lt' THEN phractxt_id ELSE NULL END) AS "Lithuanian_Id",
       max(CASE WHEN lg = 'la' THEN txt ELSE '' END)           AS "Latvian",
       max(CASE WHEN lg = 'la' THEN phractxt_id ELSE NULL END) AS "Latvian_Id",
       max(CASE WHEN lg = 'nl' THEN txt ELSE '' END)           AS "Dutch",
       max(CASE WHEN lg = 'nl' THEN phractxt_id ELSE NULL END) AS "Dutch_Id",
       max(CASE WHEN lg = 'pt' THEN txt ELSE '' END)           AS "Portuguese",
       max(CASE WHEN lg = 'pt' THEN phractxt_id ELSE NULL END) AS "Portuguese_Id",
       max(CASE WHEN lg = 'pl' THEN txt ELSE '' END)           AS "Polish",
       max(CASE WHEN lg = 'pl' THEN phractxt_id ELSE NULL END) AS "Polish_Id",
       max(CASE WHEN lg = 'ro' THEN txt ELSE '' END)           AS "Romanian",
       max(CASE WHEN lg = 'ro' THEN phractxt_id ELSE NULL END) AS "Romanian_Id",
       max(CASE WHEN lg = 'ru' THEN txt ELSE '' END)           AS "Russian",
       max(CASE WHEN lg = 'ru' THEN phractxt_id ELSE NULL END) AS "Russian_Id",
       max(CASE WHEN lg = 'sk' THEN txt ELSE '' END)           AS "Slovak",
       max(CASE WHEN lg = 'sk' THEN phractxt_id ELSE NULL END) AS "Slovak_Id",
       max(CASE WHEN lg = 'sl' THEN txt ELSE '' END)           AS "Slovenian",
       max(CASE WHEN lg = 'sl' THEN phractxt_id ELSE NULL END) AS "Slovenian_Id",
       max(CASE WHEN lg = 'sv' THEN txt ELSE '' END)           AS "Swedish",
       max(CASE WHEN lg = 'sv' THEN phractxt_id ELSE NULL END) AS "Swedish_Id"
FROM phrac
         INNER JOIN entr_phrac_phractxtke
                    ON phrac.phrac_id = entr_phrac_phractxtke.entr_src_id
         LEFT JOIN phractxtke ON entr_phrac_phractxtke.entr_dst_id = phractxtke.phractxtke_id
         LEFT JOIN (SELECT entr_src_id AS phractxtke_id, phractxt.txt, phractxt.phractxt_id, lg
                    FROM entr_phractxtke_phractxt epp
                             INNER JOIN phractxt ON epp.entr_dst_id = phractxt.phractxt_id AND lg IN
                                                                                               ('en', 'fr', 'bg', 'cs',
                                                                                                'da', 'de', 'et',
                                                                                                'el', 'es', 'fi', 'hr',
                                                                                                'hu', 'is', 'it',
                                                                                                'lt', 'la', 'nl', 'pt',
                                                                                                'pl', 'ro', 'ru',
                                                                                                'sk', 'sl', 'sv')
                    WHERE entr_tp = 'TRANSLATION') entxt USING (phractxtke_id)
WHERE lower(ns) LIKE lower(:user_ns) || '%'
GROUP BY phrac_id, phrac.tp, phrac.ke, phractxtke.ke
;