/*
 * 
 * Extracting the Transaltions for PIF Template.
 * Languages:
 * ('en','fr','bg','cs','da','de','et','el','es','fi','hr','hu','is','it','lt','la','nl','pt','pl','ro','ru','sk','sl','sv')
 * 
 * 
 * */

With chapOfsds as (						
select ventr_src_id as sdsdoc_id, ventr_tp as linktype, sdschap.*, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
from fventr_sdsdoc_sdschap_subsdschap(ARRAY[10001])						
inner join sdschap on sdschap_id = ventr_dst_id						
UNION ALL						
select entr_src_id as sdsdoc_id, entr_tp as linktype, sdschap.*, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
from entr_sdsdoc_sdschap						
inner join sdschap on sdschap_id = entr_dst_id						
where entr_src_id  =10001 --PIF TEMPLATE						
)												
select sdsdoc_id, numero, sdschap_id, heading.phractxtke_id, heading.ke,
en.txt as "English",     en.phractxt_id as English_id,   
fr.txt as "French",      fr.phractxt_id as French_id, 
bg.txt as "Bulgarian",   bg.phractxt_id as Bulgarian_id, 
cs.txt as "Czech",       cs.phractxt_id as Czech_id, 
da.txt as "Danish",      da.phractxt_id as Danish_id, 
de.txt as "German",      de.phractxt_id as German_id, 
et.txt as "Estonian",    et.phractxt_id as Estonian_id, 
el.txt as "Greek",       el.phractxt_id as Greek_id, 
es.txt as "Spanish",     es.phractxt_id as Spanish_id, 
fi.txt as "Finnish",     fi.phractxt_id as Finnish_id, 
hr.txt as "Croatian",    hr.phractxt_id as Croatian_id, 
hu.txt as "Hungarian",   hu.phractxt_id as Hungarian_id, 
is_.txt as "Icelandic",  is_.phractxt_id as Icelandic_id,   
it.txt as "Italian",     it.phractxt_id as Italian_id, 
lt.txt as "Lithuanian",  lt.phractxt_id as Lithuanian_id, 
la.txt as "Latvian",     la.phractxt_id as Latvian_id, 
nl.txt as "Dutch",       nl.phractxt_id as Dutch_id, 
pt.txt as "Portuguese",  pt.phractxt_id as Portuguese_id, 
ro.txt as "Romanian",    ro.phractxt_id as Romanian_id, 
ru.txt as "Russian",     ru.phractxt_id as Russian_id, 
sk.txt as "Slovak",      sk.phractxt_id as Slovak_id, 
sl.txt as "Slovenian",   sl.phractxt_id as Slovenian_id, 
sv.txt as "Swedish",      sv.phractxt_id as Swedish_id  

from chapOfsds sdschap 						
 left outer join entr_sdsdoc_sdschap sdsdocparentlink on sdsdocparentlink.entr_dst_id = sdschap_id and sdsdocparentlink.entr_tp like 'CHILD_CHAPTER%' and sdsdocparentlink.entr_src_id = parentSdschap_id -- it could be only one						
 left outer join entr_sdschap_sdschap sdschapparentlink on sdschapparentlink.entr_dst_id = sdschap_id and sdschapparentlink.entr_tp like 'CHILD_CHAPTER%' and sdschapparentlink.entr_src_id = parentSdschap_id -- it could be only one						
inner join entr_sdschap_phractxtke headinglink on headinglink.entr_src_id = sdschap_id --and headinglink.entr_tp = 'HEADING' -- it could be only one for the HEADING						
inner join phractxtke heading on heading.phractxtke_id = headinglink.entr_dst_id -- it could be only one						
inner join (select distinct entr_src_id, entr_dst_id, entr_tp from entr_phrac_phractxtke where entr_tp = 'HAS') headingCatlink on headingCatlink.entr_dst_id = heading.phractxtke_id						
						
inner join phrac headingcat on headingcat.phrac_id = headingCatlink.entr_src_id and headingcat.active = 1 -- it could be only one						
inner join entr_phractxtke_phractxt enlink on enlink.entr_src_id = heading.phractxtke_id and enlink.entr_tp = 'TRANSLATION' -- it could be only one						
inner join phractxt en on en.phractxt_id = enlink.entr_dst_id and en.lg = 'en' -- it could be only one	

left outer join entr_phractxtke_phractxt frlink on frlink.entr_src_id = heading.phractxtke_id and frlink.entr_tp = 'TRANSLATION' and 'fr' = (select lg from phractxt frcheck where frcheck.phractxt_id = frlink.entr_dst_id) 					
left outer join phractxt fr on fr.phractxt_id = frlink.entr_dst_id and fr.lg = 'fr' 	

left outer join entr_phractxtke_phractxt bglink on bglink.entr_src_id = heading.phractxtke_id and bglink.entr_tp = 'TRANSLATION' and 'bg' = (select lg from phractxt bgcheck where bgcheck.phractxt_id = bglink.entr_dst_id)
left outer join phractxt bg on bg.phractxt_id = bglink.entr_dst_id and bg.lg = 'bg' 

left outer join entr_phractxtke_phractxt cslink on cslink.entr_src_id = heading.phractxtke_id and cslink.entr_tp = 'TRANSLATION' and 'cs' = (select lg from phractxt cscheck where cscheck.phractxt_id = cslink.entr_dst_id) 					
left outer join phractxt cs on cs.phractxt_id = cslink.entr_dst_id and cs.lg = 'cs' 

left outer join entr_phractxtke_phractxt dalink on dalink.entr_src_id = heading.phractxtke_id and dalink.entr_tp = 'TRANSLATION' and 'da' = (select lg from phractxt dacheck where dacheck.phractxt_id = dalink.entr_dst_id) 					
left outer join phractxt da on da.phractxt_id = dalink.entr_dst_id and da.lg = 'da' 

left outer join entr_phractxtke_phractxt delink on delink.entr_src_id = heading.phractxtke_id and delink.entr_tp = 'TRANSLATION' and 'de' = (select lg from phractxt decheck where decheck.phractxt_id = delink.entr_dst_id) 					
left outer join phractxt de on de.phractxt_id = delink.entr_dst_id and de.lg = 'de' 

left outer join entr_phractxtke_phractxt etlink on etlink.entr_src_id = heading.phractxtke_id and etlink.entr_tp = 'TRANSLATION' and 'et' = (select lg from phractxt etcheck where etcheck.phractxt_id = etlink.entr_dst_id) 					
left outer join phractxt et on et.phractxt_id = etlink.entr_dst_id and et.lg = 'et' 

left outer join entr_phractxtke_phractxt ellink on ellink.entr_src_id = heading.phractxtke_id and ellink.entr_tp = 'TRANSLATION' and 'el' = (select lg from phractxt elcheck where elcheck.phractxt_id = ellink.entr_dst_id) 					
left outer join phractxt el on el.phractxt_id = ellink.entr_dst_id and el.lg = 'el' 

left outer join entr_phractxtke_phractxt eslink on eslink.entr_src_id = heading.phractxtke_id and eslink.entr_tp = 'TRANSLATION' and 'es' = (select lg from phractxt escheck where escheck.phractxt_id = eslink.entr_dst_id) 					
left outer join phractxt es on es.phractxt_id = eslink.entr_dst_id and es.lg = 'es' 

left outer join entr_phractxtke_phractxt filink on filink.entr_src_id = heading.phractxtke_id and filink.entr_tp = 'TRANSLATION' and 'fi' = (select lg from phractxt ficheck where ficheck.phractxt_id = filink.entr_dst_id) 					
left outer join phractxt fi on fi.phractxt_id = filink.entr_dst_id and fi.lg = 'fi' 

left outer join entr_phractxtke_phractxt hrlink on hrlink.entr_src_id = heading.phractxtke_id and hrlink.entr_tp = 'TRANSLATION' and 'hr' = (select lg from phractxt hrcheck where hrcheck.phractxt_id = hrlink.entr_dst_id) 					
left outer join phractxt hr on hr.phractxt_id = hrlink.entr_dst_id and hr.lg = 'hr' 

left outer join entr_phractxtke_phractxt hulink on hulink.entr_src_id = heading.phractxtke_id and hulink.entr_tp = 'TRANSLATION' and 'hu' = (select lg from phractxt hucheck where hucheck.phractxt_id = hulink.entr_dst_id) 					
left outer join phractxt hu on hu.phractxt_id = hulink.entr_dst_id and hu.lg = 'hu' 

left outer join entr_phractxtke_phractxt islink on islink.entr_src_id = heading.phractxtke_id and islink.entr_tp = 'TRANSLATION' and 'is' = (select lg from phractxt ischeck where ischeck.phractxt_id = islink.entr_dst_id) 					
left outer join phractxt is_ on is_.phractxt_id = islink.entr_dst_id and is_.lg = 'is' 

left outer join entr_phractxtke_phractxt itlink on itlink.entr_src_id = heading.phractxtke_id and itlink.entr_tp = 'TRANSLATION' and 'it' = (select lg from phractxt itcheck where itcheck.phractxt_id = itlink.entr_dst_id) 					
left outer join phractxt it on it.phractxt_id = itlink.entr_dst_id and it.lg = 'it' 

left outer join entr_phractxtke_phractxt ltlink on ltlink.entr_src_id = heading.phractxtke_id and ltlink.entr_tp = 'TRANSLATION' and 'lt' = (select lg from phractxt ltcheck where ltcheck.phractxt_id = ltlink.entr_dst_id) 					
left outer join phractxt lt on lt.phractxt_id = ltlink.entr_dst_id and lt.lg = 'lt' 

left outer join entr_phractxtke_phractxt lalink on lalink.entr_src_id = heading.phractxtke_id and lalink.entr_tp = 'TRANSLATION' and 'la' = (select lg from phractxt lacheck where lacheck.phractxt_id = lalink.entr_dst_id) 					
left outer join phractxt la on la.phractxt_id = lalink.entr_dst_id and la.lg = 'la' 

left outer join entr_phractxtke_phractxt nllink on nllink.entr_src_id = heading.phractxtke_id and nllink.entr_tp = 'TRANSLATION' and 'nl' = (select lg from phractxt nlcheck where nlcheck.phractxt_id = nllink.entr_dst_id) 					
left outer join phractxt nl on nl.phractxt_id = nllink.entr_dst_id and nl.lg = 'nl' 

left outer join entr_phractxtke_phractxt ptlink on ptlink.entr_src_id = heading.phractxtke_id and ptlink.entr_tp = 'TRANSLATION' and 'pt' = (select lg from phractxt ptcheck where ptcheck.phractxt_id = ptlink.entr_dst_id) 					
left outer join phractxt pt on pt.phractxt_id = ptlink.entr_dst_id and pt.lg = 'pt' 

left outer join entr_phractxtke_phractxt pllink on pllink.entr_src_id = heading.phractxtke_id and pllink.entr_tp = 'TRANSLATION' and 'pl' = (select lg from phractxt plcheck where plcheck.phractxt_id = pllink.entr_dst_id) 					
left outer join phractxt pl on pl.phractxt_id = pllink.entr_dst_id and pl.lg = 'pl' 

left outer join entr_phractxtke_phractxt rolink on rolink.entr_src_id = heading.phractxtke_id and rolink.entr_tp = 'TRANSLATION' and 'ro' = (select lg from phractxt rocheck where rocheck.phractxt_id = rolink.entr_dst_id) 					
left outer join phractxt ro on ro.phractxt_id = rolink.entr_dst_id and ro.lg = 'ro' 

left outer join entr_phractxtke_phractxt rulink on rulink.entr_src_id = heading.phractxtke_id and rulink.entr_tp = 'TRANSLATION' and 'ru' = (select lg from phractxt rucheck where rucheck.phractxt_id = rulink.entr_dst_id) 					
left outer join phractxt ru on ru.phractxt_id = rulink.entr_dst_id and ru.lg = 'ru' 

left outer join entr_phractxtke_phractxt sklink on sklink.entr_src_id = heading.phractxtke_id and sklink.entr_tp = 'TRANSLATION' and 'sk' = (select lg from phractxt skcheck where skcheck.phractxt_id = sklink.entr_dst_id) 					
left outer join phractxt sk on sk.phractxt_id = sklink.entr_dst_id and sk.lg = 'sk' 

left outer join entr_phractxtke_phractxt sllink on sllink.entr_src_id = heading.phractxtke_id and sllink.entr_tp = 'TRANSLATION' and 'sl' = (select lg from phractxt slcheck where slcheck.phractxt_id = sllink.entr_dst_id) 					
left outer join phractxt sl on sl.phractxt_id = sllink.entr_dst_id and sl.lg = 'sl' 

left outer join entr_phractxtke_phractxt svlink on svlink.entr_src_id = heading.phractxtke_id and svlink.entr_tp = 'TRANSLATION' and 'sv' = (select lg from phractxt svcheck where svcheck.phractxt_id = svlink.entr_dst_id) 					
left outer join phractxt sv on sv.phractxt_id = svlink.entr_dst_id and sv.lg = 'sv' 


where sdschap_id < 2000000						
and sdschap.tp = 'TEMPLATE'						
and inheritedDeep <= 1						
order by sdsdoc_id, numero, sdschap_id


select * from phrac
inner join entr