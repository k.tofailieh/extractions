With chapOfsds as (
select ventr_src_id as sdsdoc_id, ventr_tp as linktype, sdschap.*, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep
from fventr_sdsdoc_sdschap_subsdschap(ARRAY[10001])
inner join sdschap on sdschap_id = ventr_dst_id
UNION ALL
select entr_src_id as sdsdoc_id, entr_tp as linktype, sdschap.*, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep
from entr_sdsdoc_sdschap
inner join sdschap on sdschap_id = entr_dst_id
where entr_src_id  =10001 --PIF TEMPLATE
)
select sdsdoc_id,
       numero,
       sdschap_id,
       heading.phractxtke_id,
       heading.ke,
       translation_.phractxt_id,
       translation_.lg,
       translation_.txt
from chapOfsds sdschap
     left outer join entr_sdsdoc_sdschap sdsdocparentlink
                     on sdsdocparentlink.entr_dst_id = sdschap_id and sdsdocparentlink.entr_tp like 'CHILD_CHAPTER%' and
                        sdsdocparentlink.entr_src_id = parentSdschap_id -- it could be only one
     left outer join entr_sdschap_sdschap sdschapparentlink on sdschapparentlink.entr_dst_id = sdschap_id and
                                                               sdschapparentlink.entr_tp like 'CHILD_CHAPTER%' and
                                                               sdschapparentlink.entr_src_id =
                                                               parentSdschap_id -- it could be only one
     inner join      entr_sdschap_phractxtke headinglink
                     on headinglink.entr_src_id = sdschap_id --and headinglink.entr_tp = 'HEADING' -- it could be only one for the HEADING
     inner join      phractxtke heading on heading.phractxtke_id = headinglink.entr_dst_id -- it could be only one
     inner join      (select distinct entr_src_id, entr_dst_id, entr_tp
                      from entr_phrac_phractxtke
                      where entr_tp = 'HAS') headingCatlink on headingCatlink.entr_dst_id = heading.phractxtke_id

     inner join      phrac headingcat on headingcat.phrac_id = headingCatlink.entr_src_id and
                                         headingcat.active = 1 -- it could be only one
     inner join      entr_phractxtke_phractxt enlink on enlink.entr_src_id = heading.phractxtke_id and
                                                        enlink.entr_tp = 'TRANSLATION' -- it could be only one
     inner join      phractxt translation_
                     on translation_.phractxt_id = enlink.entr_dst_id and translation_.lg in ('en')--,'fr','bg','cs','da','de','et','el','es','fi','hr','hu','is','it','lt','la','nl','pt','pl','ro','ru','sk','sl','sv') -- it could be only one
where lower(heading.ke) like '%information%'
      --sdschap.tp = 'TEMPLATE'
-- and inheritedDeep <= 1
order by sdsdoc_id, lg, numero, sdschap_id


select *
from tm
where tu_ke like 'This_information_i%'
;

-- original: The information given in this Safety Data Sheet is based on our present knowledge and on European and national regulations.
-- original charonly = THE INFORMATION GIVEN IN THIS SAFETY DATA SHEET IS BASED ON OUR PRESENT KNOWLEDGE AND ON EUROPEAN AND NATIONAL REGULATIONS


-- new: The information given in this Safety Data Sheet is based on our present knowledge and national regulations.
update tm
set tuv_seg      = 'The information given in this Safety Data Sheet is based on our present knowledge and national regulations.',
    tuv_charonly = 'THE INFORMATION GIVEN IN THIS SAFETY DATA SHEET IS BASED ON OUR PRESENT KNOWLEDGE AND NATIONAL REGULATIONS'
where tm_id = 325640;

select * from phractxtke where ke like 'This_information_i%'

select *
from txt --where txt_ke like 'This_information_i%'
;

select  t.* from txtke
         inner join entr_txtke_txt ett on txtke.txtke_id = ett.entr_src_id
            inner join txt t on ett.entr_dst_id = t.txt_id
         where ke  like 'This_information_i%'
;



-- original: The information given in this Safety Data Sheet is based on our present knowledge and on European and national regulations.
update txt set txt = 'The information given in this Safety Data Sheet is based on our present knowledge and national regulations.'
where txt_id = 121271
;


select m_ref, cust_id, marguage from fiche where tp = 'RAW_MATERIAL' and coalesce(m_ref, cust_id, marguage, '') != '';


select *
from phrac
where ke  like 'This_information_i%';


select * from txtke where txtke.ke like '%This_information_i%'

select distinct tp from txtke