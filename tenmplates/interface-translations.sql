SELECT txtke.txtke_id,
       txtke.ke,
       txtke.bundle AS application,
       entxt.txt_id AS english_id,
       entxt.txt    AS english,
       kotxt.txt_id AS korean_id,
       kotxt.txt    AS korean,
       jatxt.txt_id AS japanese_id,
       jatxt.txt    AS japanese
FROM txtke
         LEFT JOIN (SELECT entr_src_id AS txtke_id, txt.txt_id, txt.txt
                    FROM entr_txtke_txt ett
                             INNER JOIN txt ON ett.entr_dst_id = txt_id AND lg = 'en'
                    WHERE entr_tp = 'TRANSLATION') entxt USING (txtke_id)
         LEFT JOIN (SELECT entr_src_id AS txtke_id, txt.txt_id, txt.txt
                    FROM entr_txtke_txt ett
                             INNER JOIN txt ON ett.entr_dst_id = txt_id AND lg = 'ko'
                    WHERE entr_tp = 'TRANSLATION') kotxt USING (txtke_id)
         LEFT JOIN (SELECT entr_src_id AS txtke_id, txt.txt_id, txt.txt
                    FROM entr_txtke_txt ett
                             INNER JOIN txt ON ett.entr_dst_id = txt_id AND lg = 'ja'
                    WHERE entr_tp = 'TRANSLATION') jatxt USING (txtke_id)
WHERE txtke.ns = '*';


------------------------------------------------------------------------------------------------------------------------

SELECT txtke.txtke_id,
       txtke.ke,
       txtke.bundle AS application,
       entxt.txt_id AS english_id,
       entxt.txt    AS english,
       kotxt.txt_id AS korean_id,
       kotxt.txt    AS korean,
       jatxt.txt_id AS japanese_id,
       jatxt.txt    AS japanese
FROM txtke
         LEFT JOIN (SELECT entr_src_id AS txtke_id, txt.txt_id, txt.txt, lg
                    FROM entr_txtke_txt ett
                             INNER JOIN txt ON ett.entr_dst_id = txt_id AND lg IN ('en')
                    WHERE entr_tp = 'TRANSLATION') entxt USING (txtke_id)
WHERE txtke.ns = '*';
;






