/*
 * This Extaction follows the MCD: https://docs.google.com/drawings/d/16mOhEX7TSXDOHRyRIQwbFXNRpN5XBcmHuM5dYyoo0Sg/edit?usp=sharing
 * 
 * Extract PIF Translations: for all European languages.
 * There are three parts to the extraction query:
 * -1st part is about getting the PIFs and sdsdoc that related to.
 * -2nd part is about getting the chapters hierarchy that related to sdsdoc.
 * -3rd part is about getting the translations for the chapters.
 * */

WITH pif_docs AS (
	/* get PIF and sds_doc that related to.*/
	SELECT m_id AS pif_id, m_ref AS pif_name, efs.entr_dst_id AS doc_id , "name" AS doc_name
	FROM fiche
	INNER JOIN entr_fiche_sdsdoc efs ON efs.entr_src_id = m_id AND efs.entr_tp = 'PIF'
	INNER JOIN sdsdoc ON sdsdoc_id = efs.entr_dst_id 
), 
pif_ids AS(
	/* get PIF id from the relation.*/
	SELECT  entr_dst_id AS doc_id FROM entr_fiche_sdsdoc WHERE entr_tp = 'PIF'
), 
chapters_tree AS (
    /* get all chapters that related to PIF documents.*/
	SELECT ventr_src_id AS doc_id, ventr_tp AS tp, ventr_dst_id AS chap_id, ventr_num1 AS parent_chap_id, ventr_st AS st, ventr_char2 AS numero	
	FROM fventr_sdsdoc_sdschap_subsdschap((SELECT array_agg(doc_id) FROM pif_ids))
	UNION ALL	/* to get parent sdschap.*/
	SELECT entr_src_id AS doc_id, entr_tp AS tp, entr_dst_id chap_id, entr_num1 AS parent_chap_id, entr_st AS st, entr_char2 AS numero	
	FROM entr_sdsdoc_sdschap						
	INNER JOIN  sdschap ON sdschap_id = entr_dst_id						
	WHERE entr_src_id IN (SELECT doc_id FROM pif_ids)
),
translations AS (
 	/* get translations we can filter over the lg.*/
	SELECT chapters_tree.doc_id, chapters_tree.numero , trs.ke, trs.txt AS "Translation", trs.lg
	FROM chapters_tree
	INNER JOIN ( /* get the phractxts. */
				SELECT esp.entr_src_id AS chap_id, p.ke, ptxt.lg, ptxt.txt 
				FROM  entr_sdschap_phractxtke esp 
				INNER JOIN phractxtke p ON p.phractxtke_id= esp.entr_dst_id AND esp.entr_tp = 'HEADING'
				INNER JOIN entr_phractxtke_phractxt epp ON p.phractxtke_id = epp.entr_src_id AND epp.entr_tp = 'TRANSLATION'
				INNER JOIN phractxt ptxt ON ptxt.phractxt_id = epp.entr_dst_id ) trs 
				ON trs.chap_id = chapters_tree.chap_id AND trs.lg IN ('en','fr','bg','cs','da','de','et','el','es','fi','hr','hu','is','it','lt','la','nl','pt','pl','ro','ru','sk','sl','sv')
	ORDER BY chapters_tree.doc_id, chapters_tree.numero, trs.lg
)
SELECT pif_id, pif_name,doc_id, doc_name, numero, ke, "Translation", lg FROM translations
INNER JOIN pif_docs USING(doc_id);


------------------------------------------------------------------------------------------------------------------------------------------------------


With chapOfsds as (						
select ventr_src_id as sdsdoc_id, ventr_tp as linktype, sdschap.*, ventr_num1 as parentSdschap_id, ventr_num2 as deep, ventr_float2 as inheritedDeep						
from fventr_sdsdoc_sdschap_subsdschap(ARRAY[10001])						
inner join sdschap on sdschap_id = ventr_dst_id						
UNION ALL						
select entr_src_id as sdsdoc_id, entr_tp as linktype, sdschap.*, entr_src_id as parentSdschap_id, 0 as deep, CAST((CASE entr_tp WHEN 'CHILD_CHAPTER_INHERITED' THEN 1 ELSE 0 END) AS double precision) AS inheritedDeep						
from entr_sdsdoc_sdschap						
inner join sdschap on sdschap_id = entr_dst_id						
where entr_src_id  =10001 --PIF TEMPLATE						
)												
select sdsdoc_id, numero, sdschap_id, heading.phractxtke_id, heading.ke, translation_.phractxt_id,translation_.lg, translation_.txt				
from chapOfsds sdschap 						
 left outer join entr_sdsdoc_sdschap sdsdocparentlink on sdsdocparentlink.entr_dst_id = sdschap_id and sdsdocparentlink.entr_tp like 'CHILD_CHAPTER%' and sdsdocparentlink.entr_src_id = parentSdschap_id -- it could be only one						
 left outer join entr_sdschap_sdschap sdschapparentlink on sdschapparentlink.entr_dst_id = sdschap_id and sdschapparentlink.entr_tp like 'CHILD_CHAPTER%' and sdschapparentlink.entr_src_id = parentSdschap_id -- it could be only one						
inner join entr_sdschap_phractxtke headinglink on headinglink.entr_src_id = sdschap_id --and headinglink.entr_tp = 'HEADING' -- it could be only one for the HEADING						
inner join phractxtke heading on heading.phractxtke_id = headinglink.entr_dst_id -- it could be only one						
inner join (select distinct entr_src_id, entr_dst_id, entr_tp from entr_phrac_phractxtke where entr_tp = 'HAS') headingCatlink on headingCatlink.entr_dst_id = heading.phractxtke_id						
						
inner join phrac headingcat on headingcat.phrac_id = headingCatlink.entr_src_id and headingcat.active = 1 -- it could be only one						
inner join entr_phractxtke_phractxt enlink on enlink.entr_src_id = heading.phractxtke_id and enlink.entr_tp = 'TRANSLATION' -- it could be only one						
inner join phractxt translation_ on translation_.phractxt_id = enlink.entr_dst_id and translation_.lg IN ('en','fr','bg','cs','da','de','et','el','es','fi','hr','hu','is','it','lt','la','nl','pt','pl','ro','ru','sk','sl','sv') -- it could be only one						
						
where sdschap_id < 2000000						
and sdschap.tp = 'TEMPLATE'						
and inheritedDeep <= 1						
order by sdsdoc_id,lg, numero, sdschap_id	


























