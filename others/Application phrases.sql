select distinct app, bundle
from txtke
     inner join entr_txtke_txt ett on txtke.txtke_id = ett.entr_src_id
     inner join txt t on ett.entr_dst_id = t.txt_id;
------------------------------------------------------------------------------------------------------------------------
/**/
select
txtke.txtke_id,
       txtke.ke,
       txtke.bundle as application,
       entxt.txt_id as english_id,
       entxt.txt    as english,
       kotxt.txt_id as italian_id,
       kotxt.txt    as italian
from txtke
     left join (select entr_src_id as txtke_id, txt.txt_id, txt.txt
                from entr_txtke_txt ett
                     inner join txt on ett.entr_dst_id = txt_id and lg = 'en'
                where entr_tp = 'TRANSLATION') entxt using (txtke_id)
     left join (select entr_src_id as txtke_id, txt.txt_id, txt.txt
                from entr_txtke_txt ett
                     inner join txt on ett.entr_dst_id = txt_id and lg = 'it'
                where entr_tp = 'TRANSLATION') kotxt using (txtke_id)
where txtke.ns = '*'
order by application, english nulls  last , italian nulls last ;


------------------------------------------------------------------------------------------------------------------------
with phrases_to_imporve(ke, lg, val) as (values ('My Dashboard', 'en', ''),
                                                ('Products', 'en', ''),
                                                ('Labelling', 'en', ''),
                                                ('Impurity', 'en', ''),
                                                ('Eco Market', 'en', ''),
                                                ('Projects', 'en', ''),
                                                ('Batch', 'en', ''),
                                                ('Pilot Batch', 'en', ''),
                                                ('Equipments', 'en', ''),
                                                ('Test', 'en', ''),
                                                ('Batch concentré parfumé', 'fr', ''),
                                                ('Conformité parfum', 'fr', ''),
                                                ('Mise en marché international', 'fr', ''),
                                                ('Veille FormulaCheck', 'fr', ''),
                                                ('Cosmetovigilance', 'fr', ''),
                                                ('Portfolio', 'fr', ''),
                                                ('Règlements', 'fr', '')),
    txtke_term as (
        select * from txtke
                 inner join entr_txtke_txt e on txtke.txtke_id = e.entr_src_id

    )
select phrases_to_imporve.ke,txtke.txtke_id as key_id,  txtke.ke as key, txt.txt_id as translation_id, txt.txt as translation_value,txt.lg as lg, it.txt_id as italian_id, it.txt as italoan_value
from phrases_to_imporve
     left join txt on trim(ke) = trim(txt.txt) and phrases_to_imporve.lg = txt.lg
     left join entr_txtke_txt ett on txt.txt_id = ett.entr_dst_id
     left join txtke on ett.entr_src_id = txtke.txtke_id
     inner join entr_txtke_txt ett1 on txtke_id = ett1.entr_src_id
     inner join txt it on ett1.entr_dst_id = it.txt_id and it.lg = 'it'
order by phrases_to_imporve.ke
;



select * from txt where txt ilike '%règlements%' and lg = 'fr'

/**/
select txtke.txtke_id,
       txtke.ke,
       txtke.bundle as application,
       entxt.txt_id as english_id,
       entxt.txt    as english,
       kotxt.txt_id as korean_id,
       kotxt.txt    as korean,
       jatxt.txt_id as japanese_id,
       jatxt.txt    as japanese
from txtke
     left join (select entr_src_id as txtke_id, txt.txt_id, txt.txt
                from entr_txtke_txt ett
                     inner join txt on ett.entr_dst_id = txt_id and lg = 'en'
                where entr_tp = 'TRANSLATION') entxt using (txtke_id)
     left join (select entr_src_id as txtke_id, txt.txt_id, txt.txt
                from entr_txtke_txt ett
                     inner join txt on ett.entr_dst_id = txt_id and lg = 'ko'
                where entr_tp = 'TRANSLATION') kotxt using (txtke_id)
     left join (select entr_src_id as txtke_id, txt.txt_id, txt.txt
                from entr_txtke_txt ett
                     inner join txt on ett.entr_dst_id = txt_id and lg = 'ja'
                where entr_tp = 'TRANSLATION') jatxt using (txtke_id)
where txtke.ns = '*'
